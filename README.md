# Python client for Ockto API

Currently supports getting token for username and password, data source availability and starting the Ockto session.

## Tests

To run the tests you need the access to Ockto API staging environment. Tests use following environment variables:

- `O_USERNAME`: your Ockto API username
- `O_PASSWORD`: your Ockto API password
- `O_PROVIDER`: provider to use
- `O_CERT`: path to your certificate
- `O_CERT_KEY`: path to your certificate key

When provided, you can run the tests with following command: `pytest --workers auto ockto/tests.py`

All dependencies with pytest and pytest-parallel are included in `requirements.txt`.
