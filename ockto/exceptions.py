class ArgumentError(Exception):
    pass


class CertificateError(Exception):
    pass


class ResponseError(Exception):
    pass
