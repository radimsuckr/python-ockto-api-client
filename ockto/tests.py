import tempfile
import uuid
from os import environ

import pytest

from ockto import Ockto, exceptions

O_USERNAME = environ.get('O_USERNAME')
O_PASSWORD = environ.get('O_PASSWORD')
O_PROVIDER = environ.get('O_PROVIDER')
O_CERT = environ.get('O_CERT')
O_CERT_KEY = environ.get('O_CERT_KEY')


def test_init_api_mode_and_cert():
        Ockto('SANDBOX')
        Ockto('STAGE')
        Ockto('PRODUCTION')
        with pytest.raises(exceptions.ArgumentError):
            Ockto('invalid')
        with pytest.raises(exceptions.CertificateError) as e:
            Ockto('STAGE', 42)
            assert str(e) == '"cert" argument has to be 2-valued list, tuple or set'
        with pytest.raises(exceptions.CertificateError) as e:
            Ockto(cert=('qwe'))
            assert str(e) == '"cert" argument has to be 2-valued list, tuple or set'
        with pytest.raises(exceptions.CertificateError) as e:
            Ockto(cert=('qwe', 'asd'))
            assert str(e) == 'Path "qwe" does not exist'
        with pytest.raises(exceptions.CertificateError) as e:
            Ockto(cert=(O_CERT, 'asd'))
            assert str(e) == 'Path "asd" does not exist'
        with pytest.raises(exceptions.CertificateError) as e, tempfile.TemporaryDirectory() as tmpdir:
            Ockto(cert=(tmpdir, tmpdir))
            assert str(e) == 'Path "{tmpdir}" is not file'
        with pytest.raises(exceptions.CertificateError) as e, tempfile.TemporaryDirectory() as tmpdir:
            Ockto(cert=(O_CERT, tmpdir))
            assert str(e) == 'Path "{tmpdir}" is not file'
        with pytest.raises(exceptions.CertificateError) as e, tempfile.TemporaryDirectory() as tmpdir:
            Ockto(cert=(tmpdir, O_CERT_KEY))
            assert str(e) == 'Path "{tmpdir}" is not file'


@pytest.fixture
def ockto():
    return Ockto(cert=(O_CERT, O_CERT_KEY))


def test_get_token_for_username(ockto):
    result = ockto.get_token_for_username_password(O_USERNAME, O_PASSWORD, O_PROVIDER)
    assert type(result) == str


def test_get_data_source_availability(ockto):
    token = ockto.get_token_for_username_password(O_USERNAME, O_PASSWORD, O_PROVIDER)
    result = ockto.get_data_source_availability(token)
    assert type(result) == list
    for item in result:
        assert type(item) == dict
        assert set(item.keys()) == {'datasource', 'availability', 'Id'}


def test_start_ockto_session_only_with_required_parameters(ockto):
    result = ockto.start_ockto_session(str(uuid.uuid4()), 'BRI', 'O')
    assert type(result) == list


def test_start_ockto_session_only_with_invalid_kwarg(ockto):
    with pytest.raises(exceptions.ArgumentError) as e:
        ockto.start_ockto_session(str(uuid.uuid4()), 'BRI', 'O', invalid_kwarg=42)
        assert str(e) == "Invalid arguments given: {'invalid_kwarg'}"
