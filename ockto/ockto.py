from pathlib import Path

import inflection
import requests

from ockto.exceptions import ArgumentError, CertificateError, ResponseError


class Ockto:
    """
    Main class used for communication with Ockto API. It supports all three API modes (sandbox, stage, production).
    """

    API_MODES = {'SANDBOX', 'STAGE', 'PRODUCTION'}
    API_URLS = {
        'SANDBOX': 'https://api.ockto.nl/v1.0/OCK_SANDBOX/Ockto/Static-OcktoService',
        'STAGE': 'https://api.ockto.nl/v1.0/OCK_STAGE/Ockto/Static-OcktoService',
        'PRODUCTION': 'https://api.ockto.nl/v1.0/OCK_PROD/Ockto/Static-OcktoService'
    }

    def __init__(self, api_mode='STAGE', cert=None):
        if api_mode not in self.API_MODES:
            raise ArgumentError(f'"api_mode" has to be one of following values: {self.API_MODES}')
        self.api_mode = api_mode
        self.base_url = self.API_URLS[self.api_mode]
        self.cert = None
        if cert:
            if type(cert) not in {list, tuple, set} or len(cert) != 2:
                raise CertificateError('"cert" argument has to be 2-valued list, tuple or set')
            for path_value in cert:
                path = Path(path_value)
                if not path.exists():
                    raise CertificateError(f'Path "{path}" does not exist')
                if not path.is_file():
                    raise CertificateError(f'Path "{path}" is not file')
            self.cert = cert

    def _handle_response(self, response):
        data = response.json()
        if type(data) == dict:
            if 'HasError' in data.keys() and data.get('HasError'):
                error = data['ResultMessage']
                raise ResponseError(f'Error from Ockto API: {error}')
            if 'ErrorDescription' in data.keys():
                error = data['ErrorDescription']
                raise ResponseError(f'Error from Ockto API: {error}')
        return data

    def get_token_for_username_password(self, username, password, provider):
        """Returns the token for username and password."""

        URL = f'{self.base_url}/GetTokenForUserNamePassword/'
        kwargs = {
            'json': {
                'UserName': username,
                'Password': password,
                'Provider': provider,
            },
        }
        if self.cert:
            kwargs['cert'] = self.cert
        response = requests.post(URL, **kwargs)
        json = self._handle_response(response)
        return json['Result']

    def get_data_source_availability(self, token):
        """Returns data source availability for given token."""

        URL = f'{self.base_url}/GetDataSourceAvailability/'
        kwargs = {
            'headers': {
                'Content-Type': 'application/json',
                'CordFinance-Auth-Token': token,
            },
        }
        if self.cert:
            kwargs['cert'] = self.cert
        response = requests.get(URL, **kwargs)
        json = self._handle_response(response)
        return json

    def start_ockto_session(self, id, provider, ockto_type, **kwargs):
        """
        Starts new Ockto session.

        The required and optional parameters mirror Ockto documentation and API. If the method gets any argument that
        isn't part of the Ockto API documentation it raises an "ArgumentError".
        """
        URL = f'{self.base_url}/StartOcktoSession/'
        optional_args = {'cdm_version', 'requested_documents', 'mobile2mobile', 'test_case_id', 'consent_code',
                         'odm_set_name', 'disable_partner', 'remote_ip_address', 'os_name', 'os_version'}
        kwargs_keys = set(kwargs.keys())
        if not kwargs_keys.issubset(optional_args):
            raise ArgumentError(f'Invalid arguments given: {kwargs_keys - optional_args}')
        kwargs = {
            'json': {
                'Id': id,
                'Provider': provider,
                'OcktoType': ockto_type,
                **{inflection.camelize(key, True): value for key, value in kwargs.items()},
            },
        }
        if self.cert:
            kwargs['cert'] = self.cert
        response = requests.post(URL, **kwargs)
        return self._handle_response(response)['Result']
