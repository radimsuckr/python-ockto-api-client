import setuptools

with open('README.md', 'r') as file:
    long_description = file.read()

setuptools.setup(
    name='ockto_api_client',
    version='0.0.1',
    author='Radim Sückr',
    author_email='contact@radimsuckr.cz',
    description='Python client for Ockto API',
    license='MIT',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/radimsuckr/python-ockto-api-client',
    packages=setuptools.find_packages(exclude=('tests',)),
    install_requires=(
        'requests',
        'inflection',
    ),
    python_requires='>=3.6',
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ]
)
